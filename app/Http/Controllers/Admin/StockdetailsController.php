<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Stockdetail;
use Illuminate\Http\Request;
use App\Authorizable;

class StockdetailsController extends Controller
{
      use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $stockdetails = Stockdetail::where('company_name', 'LIKE', "%$keyword%")
                ->orWhere('category', 'LIKE', "%$keyword%")
                ->orWhere('price', 'LIKE', "%$keyword%")
                ->orWhere('target', 'LIKE', "%$keyword%")
                ->orWhere('stoploss', 'LIKE', "%$keyword%")
                ->orWhere('company_logo', 'LIKE', "%$keyword%")
                ->orWhere('image_chart', 'LIKE', "%$keyword%")
                ->orWhere('stock_state', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $stockdetails = Stockdetail::latest()->paginate($perPage);
        }

        return view('admin.stockdetails.index', compact('stockdetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.stockdetails.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'company_name' => 'required',
            'category' => 'required',
            'price' => 'required',
            'target' => 'required',
            'stoploss' => 'required',
            'description' => 'required'
        ]);
        
        $requestData = $request->all();
        if ($request->hasFile('company_logo')) {
            $filename = $this->getFileName($request->company_logo);
            $request->company_logo->move(base_path('public/CompanyLogo'), $filename);
            $requestData['company_logo']=$filename;
        }
        if ($request->hasFile('image_chart')) {
            $filename = $this->getFileName($request->image_chart);
            $request->image_chart->move(base_path('public/ImageChart'), $filename);
            $requestData['image_chart']=$filename;
        }

        Stockdetail::create($requestData);

        return redirect('/admin/stockdetails')->with('flash_message', 'Stockdetail added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $stockdetail = Stockdetail::findOrFail($id);

        return view('admin.stockdetails.show', compact('stockdetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $stockdetail = Stockdetail::findOrFail($id);

        return view('admin.stockdetails.edit', compact('stockdetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'company_name' => 'required',
            'category' => 'required',
            'price' => 'required',
            'target' => 'required',
            'stoploss' => 'required',
            'description' => 'required'
        ]);
        
        $requestData = $request->all();

        if ($request->hasFile('company_logo')) {
            $filename = $this->getFileName($request->company_logo);
            $request->company_logo->move(base_path('public/CompanyLogo'), $filename);
            $requestData['company_logo']=$filename;
        }
        if ($request->hasFile('image_chart')) {
            $filename = $this->getFileName($request->image_chart);
            $request->image_chart->move(base_path('public/ImageChart'), $filename);
            $requestData['image_chart']=$filename;
        }

        $stockdetail = Stockdetail::findOrFail($id);
        $stockdetail->update($requestData);

        return redirect('admin/stockdetails')->with('flash_message', 'Stockdetail updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Stockdetail::destroy($id);

        return redirect('admin/stockdetails')->with('flash_message', 'Stockdetail deleted!');
    }

    protected function getFileName($file)
    {
        return str_random(32) . '.' . $file->extension();
    }

    public function smsnotification(Request $request,$pushtokenString){
        if($pushtokenString!='' && !empty($pushtokenString) && !is_null($pushtokenString)){
            $pushtokenArr = explode(',',$pushtokenString);
            $data = array('application' => '65ECB-0EF0F',
                'auth' => 'nJ3jRCfC202uY16fef0bxAmTNldMGDQsAWDft12y24NN6LztcwVlBxfXBZQArqYMBoxaldQQtuujhXHRUZdD',
                'notifications' => array(array('send_date' => 'now',
                    'content' => 'test',
                    'data' => array('code' => 954, 'deletependingapprovals' => $tempmessagesidstring),
                    'link' => 'http://ediffy.com/',
                    'devices' => $pushtokenArr
                )
                ),

            );
            $url = 'https://cp.pushwoosh.com/json/1.3/createMessage';
            $request = json_encode(array('request' => $data));

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

            $response = curl_exec($ch);
            //var_dump($response);
            $info = curl_getinfo($ch);
            curl_close($ch);

        }
    }
}
