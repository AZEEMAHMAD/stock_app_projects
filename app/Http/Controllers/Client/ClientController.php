<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Client;



class ClientController extends Controller
{
    public function mobile_verification(Request $request)
    {
        $this->validate($request, [
            'mobile' => 'required|digits:10',
        ]);
        $data=$request->all();
        $user=Client::where('mobile',$data['mobile'])->first();
        if(!$user){
            $userdata['mobile']=$data['mobile'];
            $user=Client::create($userdata);
        }
        // $otp = rand(10000, 99999);
        $otp=12345;
        /************Write SMS Sending code *************/
        ///////////write code////////////
        /*************End SMS code ******************/
        $user->otp=$otp;
         $client =$user->save();
        if($client){
            $response = array("code"=>200,"data"=>null,"message"=>"Otp sent successfully.");
            return json_encode($response);
        }
        else{
            $response = array("code"=>400,"data"=>null,"message"=>"Otp sending fail. Please try again");
            return json_encode($response);
        }

    }

    public function otp_verification(Request $request)
    {
        $data=$request->all();
        $user=Client::where('mobile',$data['mobile'])->first();
        if(!$user){
            echo 0;
        }
        elseif($data['otp'] != $user->otp ){
            echo 2;
        }
        else{
            $user->mobile_verified_status=1;
            $user->mobile_verified_at=date('Y-m-d h:i:s');
            $user->save();
            echo 3;
        }
    }

}
