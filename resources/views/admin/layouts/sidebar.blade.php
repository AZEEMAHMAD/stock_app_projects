<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                @if(Auth::guard('web')->user()->image)
                    <img src="{{asset('images/profile_image'.'/'.Auth::guard('web')->user()->image)}}"
                         class="img-circle"
                         alt="User Image"/>
                @else
                    <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle"
                         alt="User Image"/>
                @endif
            </div>
            <div class="pull-left info">
                <p>{{ ucwords(Auth::guard('web')->user()->name)}}</p>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Admin-Panel</li>
            <li><a href="{{'/admin/home'}}"><span><i class="fa fa-home"></i> DashBoard</span></a></li>
            @can('view_users')
            <li><a href="{{'/admin/users'}}"><span><i class="fa fa-users"></i> Users</span></a></li>
            @endcan
            @can('view_roles')
            <li><a href="{{'/admin/roles'}}"><span><i class="fa fa-book"></i> Role</span></a></li>
            @endcan
            @can('view_stockdetails')
            <li><a href="{{'/admin/stockdetails'}}"><span><i class="fa fa-tasks"></i> Stock Details</span></a></li>
            @endcan
        </ul>
    </section>
</aside>