@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-success">
                    <div class="panel-heading">Stockdetail {{ $stockdetail->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/stockdetails') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/stockdetails/' . $stockdetail->id . '/edit') }}"
                           title="Edit Stockdetail">
                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> Edit
                            </button>
                        </a>

                        <form method="POST" action="{{ url('/admin/stockdetails' . '/' . $stockdetail->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs" title="Delete Stockdetail"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                             aria-hidden="true"></i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $stockdetail->id }}</td>
                                </tr>
                                <tr>
                                    <th> Company Name</th>
                                    <td> {{ $stockdetail->company_name }} </td>
                                </tr>
                                <tr>
                                    <th> Category</th>
                                    <td> {{ $stockdetail->category }} </td>
                                </tr>
                                <tr>
                                    <th> Price</th>
                                    <td> {{ $stockdetail->price }} </td>
                                </tr>
                                <tr>
                                    <th> Target</th>
                                    <td> {{ $stockdetail->target }} </td>
                                </tr>
                                <tr>
                                    <th> Stoploss</th>
                                    <td> {{ $stockdetail->stoploss }} </td>
                                </tr>
                                <tr>
                                    <th> Company Logo</th>
                                    <td> <img src="{{asset('CompanyLogo/'.$stockdetail->company_logo)}}"> </td>
                                </tr>
                                <tr>
                                    <th> Stock Chart</th>
                                    <td> <img src="{{asset('ImageChart/'.$stockdetail->image_chart)}}"> </td>
                                </tr>
                                <tr>
                                    <th> Stock State</th>
                                    <td>{{ ucwords($stockdetail->stock_state) }} </td>
                                </tr>
                                <tr>
                                    <th> Descriptions</th>
                                    <td>{!! $stockdetail->description !!} </td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td>{{$stockdetail->status==1?'Active':'In-Active'}} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
