<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>Stockdetail</b></div>
        <br/>

        <div class="form-group {{ $errors->has('company_name') ? 'has-error' : ''}}">
            <label for="company_name" class="col-md-2 control-label">{{ 'Company Name' }} :</label>

            <div class="col-md-8">
                <input class="form-control" name="company_name" type="text" id="company_name"
                       value="{{ isset($stockdetail->company_name) ? $stockdetail->company_name : ''}}">
                {!! $errors->first('company_name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
            <label for="category" class="col-md-2 control-label">{{ 'Category' }} :</label>

            <div class="col-md-8">
                <select name="category" class="form-control" id="category">
                    @foreach (json_decode('{"buy": "BUY", "sell": "SELL"}', true) as $optionKey => $optionValue)
                        <option value="{{ $optionKey }}" {{ (isset($stockdetail->category) && $stockdetail->category == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                    @endforeach
                </select>
                {!! $errors->first('category', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
            <label for="price" class="col-md-2 control-label">{{ 'Price' }} :</label>

            <div class="col-md-8">
                <input class="form-control" name="price" type="text" id="price"
                       value="{{ isset($stockdetail->price) ? $stockdetail->price : ''}}">
                {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('target') ? 'has-error' : ''}}">
            <label for="target" class="col-md-2 control-label">{{ 'Target' }} :</label>

            <div class="col-md-8">
                <input class="form-control" name="target" type="text" id="target"
                       value="{{ isset($stockdetail->target) ? $stockdetail->target : ''}}">
                {!! $errors->first('target', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('stoploss') ? 'has-error' : ''}}">
            <label for="stoploss" class="col-md-2 control-label">{{ 'Stoploss' }} :</label>

            <div class="col-md-8">
                <input class="form-control" name="stoploss" type="text" id="stoploss"
                       value="{{ isset($stockdetail->stoploss) ? $stockdetail->stoploss : ''}}">
                {!! $errors->first('stoploss', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('company_logo') ? 'has-error' : ''}}">
            <label for="company_logo" class="col-md-2 control-label">{{ 'Company Logo' }} :</label>

            <div class="col-md-8">
                <input class="form-control" name="company_logo" type="file" id="company_logo"
                       value="{{ isset($stockdetail->company_logo) ? $stockdetail->company_logo : ''}}">
                {!! $errors->first('company_logo', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('image_chart') ? 'has-error' : ''}}">
            <label for="image_chart" class="col-md-2 control-label">{{ 'Image Chart' }} :</label>

            <div class="col-md-8">
                <input class="form-control" name="image_chart" type="file" id="image_chart"
                       value="{{ isset($stockdetail->image_chart) ? $stockdetail->image_chart : ''}}">
                {!! $errors->first('image_chart', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        @if(isset($stockdetail))
        <div class="form-group {{ $errors->has('stock_state') ? 'has-error' : ''}}">
            <label for="stock_state" class="col-md-2 control-label">{{ 'Stock State' }} :</label>

            <div class="col-md-8">
                <select name="stock_state" class="form-control" id="stock_state" @if($stockdetail->stock_state != 'open') disabled style="background: orangered;color: white" @endif>
                    @foreach (json_decode('{"open": "OPEN", "targethit": "Target HIT","stoplosshit":"StopLoss HIT"}', true) as $optionKey => $optionValue)
                        <option value="{{ $optionKey }}" {{ (isset($stockdetail->stock_state) && $stockdetail->stock_state == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                    @endforeach
                </select>
                {!! $errors->first('stock_state', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        @endif
        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
            <label for="description" class="col-md-2 control-label">{{ 'Description' }} :</label>

            <div class="col-md-8">
                <textarea class="form-control ckeditor" rows="5" name="description" type="textarea"
                          id="description">{{ isset($stockdetail->description) ? $stockdetail->description : ''}}</textarea>
                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
            <label for="status" class="col-md-2 control-label">{{ 'Status' }} :</label>

            <div class="col-md-8">
                <select name="status" class="form-control" id="status">
                    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
                        <option value="{{ $optionKey }}" {{ (isset($stockdetail->status) && $stockdetail->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                    @endforeach
                </select>
                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-4 col-md-6">
                <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
            </div>
        </div>
    </div>
</div>
