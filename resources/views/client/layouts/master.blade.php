<!DOCTYPE html>
<html lang="en" class="no-js">
<head >
    <meta charset="utf-8">
    <title>ITI Edvest</title>
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('logo_o_zCS_icon.ico')}}">
    <meta name="description" content="An edu-focused initiative by Fortune Financial">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="https://cloud.typography.com/732108/729784/css/fonts.css">
    <link rel="stylesheet" href="{{asset('public/scripts/outdatedBrowser.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/scripts/awards.css')}}">
    <link rel="stylesheet" href="{{asset('public/scripts/mobile.min.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/css/bootstrap-modal.css">

    <!--[if lte IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('public/scripts/ie9-and-down.min.css')}}">
    <![endif]-->

    <!--[if lte IE 8]>
    <link rel="stylesheet" type="text/css" href="{{asset('public/scripts/ie8-and-down.min.css')}}">
    <![endif]-->

    <script src="{{asset('public/scripts/modernizr-2.6.2.min.js')}}"></script>
    <noscript>
        <style type="text/css">
            .menu, .newBrowser{display: none;}
            .disabledJS{display: block;}
            .c_browsers.hideBrowsers{opacity: 1;}
            .browser .statistic, .browser .download{display: block; opacity: 0; margin-left: 0; left: 0; width: 100%;}
            .browser .download a{ max-width: 210px; width: 80%;}
            .browser:hover .statistic, .browser:hover .download{opacity: 1;}


        </style>
    </noscript>

</head>
<body class="noajax  home">
    @include('client.layouts.header')
    @yield('content')
    @include('client.layouts.footer')
    <script src="{{asset('public/scripts/jquery-1.10.1.min.js')}}"></script>
    <script src="{{asset('public/scripts/outdatedBrowser.min.js')}}"></script>
    <script src="{{asset('public/scripts/xDomainRequest.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <!--[if !IE]><!-->
    <script type="text/javascript" src="{{asset('public/scripts/main.min.js')}}"></script>
    <!--<![endif]-->

    <!--[if gte IE 9]>
    <script type="text/javascript" src="{{asset('public/scripts/main.min.js')}}"></script>
    <![endif]-->
    <script>
        $(window).on('load',function(){
            $('#popup').modal('show');
        });

    </script>
@yield('scripts')
</body>
</html>