<div class="col-md-12 padding-zero">
    <div class="hamburger">
        <span class="icon_bar"></span>
        <span class="icon_bar"></span>
        <span class="icon_bar"></span>
    </div>
    <div class="navigation">
        <a href="{{url('/')}}" class="home-icon"><img src="{{asset('images/home_icons/home.png')}}"></a>
        <a href="{{url('/about')}}" class="about_icon"><img src="{{asset('images/home_icons/about_us.png')}}">&nbsp;ABOUT</a>
        <a href="{{url('/philosophy')}}" class="philosophy_icon"><img src="{{asset('images/home_icons/thinker_head.png')}}">&nbsp;PHILOSOPHY</a>
        <a href="{{url('/what-we-do')}}" class="what_icon"><img src="{{asset('images/home_icons/setting.png')}}">&nbsp;WHAT WE DO</a>
        <a href="{{url('team')}}" class="team_icon"><img src="{{asset('images/home_icons/group.png')}}">&nbsp;FOUNDING TEAM</a>
        <a href="{{url('/social')}}" class="social_icon"><img src="{{asset('images/home_icons/social_impact.png')}}">&nbsp;SOCIAL IMPACT</a>
        <a href="{{url('/irm')}}" class="contact_icon"><img src="{{asset('images/home_icons/social_impact.png')}}">&nbsp;IRM IN INDIA</a>
        {{--<a href="{{url('/career')}}" class="contact_icon"><img src="{{asset('images/home_icons/social_impact.png')}}">&nbsp;CAREER</a>--}}

        <footer class="desktop_footer">
            <div class="copyright_text">
                <p class="open_pro_link">Young Leaders' Business Risk Program <a href="{{url('/irm_program')}}" class="btn_enroll"> Enroll Now </a> </p>
                <p>&copy; 2018. The Investment Trust Of India Limited</p>
                <a href="{{url('/contact')}}" class="btn_contact">Contact Us</a>
            </div>
        </footer>
    </div>
</div>
<footer class="mobile_footer">
    <div class="copyright_text">
        <p class="open_pro_link">Young Leaders' Business Risk Program <a href="{{url('/irm_program')}}" class="btn_enroll"> Enroll Now </a> </p>

        <a href="{{url('/contact')}}" class="btn_contact">Contact Us</a>
        <p>&copy; 2018. The Investment Trust Of India Limited</p>
    </div>

</footer>